﻿var net = require('net')
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var socketio = require('socket.io');

/*-----------LOCAL NODE-------------*/

var app = express();

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

/*SERVER TCP*/
var serverTCP = net.createServer();
var serverLocalConfig=require('./Controllers/serverConfig.js')

var host1 = "http://localhost:49867";
var host2 = "http://arduinocc.azurewebsites.net";

var hostSocket1 = "http://accconnection.azurewebsites.net:80";
var hostSocket2 = "http://localhost:3001";

var remote = require('./Controllers/comRemoteServer.js');
remote.connect(hostSocket1);

var identiy = { homeControlID: null, from: 'local', socketID: null, appControlID:null , devices:[],ServerIP:null}

serverLocalConfig.isConfig().then(
    function(userdata){

        identiy.homeControlID=userdata.HomeControlID;
        identiy.appControlID=userdata.AppControlID;
        identiy.ServerIP=userdata.LocalServerIP;

        serverTCP.listen(8888, userdata.LocalServerIP, function () {

            console.log('server tcp listening on 8888');

        });

        console.log("servidor configurado");

        serverLocalConfig.CheckUserData(host2,userdata.Name,userdata.AppControlID,userdata.HomeControlID).then(
            function(result){

                remote.send('identify', identiy);

                console.log("identificacion enviada al servidor remoto");

            },
            function(err){
                console.log(err);
            }
        );
    },
    function(err){
        console.log("servidor NO configurado: "+ err);
    });


var sockets=[];
var devicesConnected=[];

remote.getSocket().on('get-devices-connection',function(){

    remote.send('connection-status',{to:identiy.appControlID,dedevicesConnected:devicesConnected});

});

remote.getSocket().on('turn-on-off',function(remotedata){

    console.log(remotedata);

    var socket = sockets[remotedata.deviceID];
    var pin=remotedata.pin.toString();
    socket.write(pin);
    //socket.write('2');

});
remote.getSocket().on('disconnect', function () {

    console.log('Web-Socket desconectado !');
    console.log('Conectando....');
    remote.connect(hostSocket1);
    remote.send('identify', identiy);

});
serverTCP.on('connection', function (socket) {

    console.log("Dispositivo conectado al servidor local tcp");

//SOCKET TCP
    socket.on('data', function (data) {
           
        var datos = ab2str(data);
        var datodsProc=datos.split("/");

        console.log(datodsProc[0]);
        console.log(datodsProc[1]);

        switch (datodsProc[0]) {

            case 'DISC':

                break;

            case 'CONECT':

                socket.id=datodsProc[1];
                sockets[socket.id]=socket;
                devicesConnected.push({deviceCon:datodsProc[1]});

                break;

            case 'ANALOG':
               console.log("DeviceID: "+datodsProc[1]);
               console.log("PotPin: "+datodsProc[2]);
               console.log("Val: "+datodsProc[3]);

                if(datodsProc[3]){

                    remote.send('analog-in',{to:identiy.appControlID,deviceID:datodsProc[1],pin:datodsProc[2],val:datodsProc[3]});

                }
         case 'TEMP':
                console.log("H: "+datodsProc[1]);
               console.log("T: "+datodsProc[2]);    
                break;
            case 'TAGID':
  
            remote.send('read-tag',{to:identiy.appControlID,tagID:datodsProc[1]});
                    
            default:

            break;
        }

    });

    socket.on('disconnect', function () {

        console.log('disconnected');
        remote.send('homeDisconect', identiy);
        remote.send('identify', identiy);

    });

    socket.on('end', function (data) {


        for (var i = 0; i < devicesConnected.length; i++) {

            if(devicesConnected[i].deviceCon == socket.id){

                devicesConnected[i]={};

            }

        }

        console.log("socket leave");

    });

    socket.on('close', function (data) {

        console.log("socket cerrado");

    });

    socket.on('error', function (error) {
        console.log("dispositivo desconectado ", error);
        socket.setTimeout(4000, function() {
            socket.connect(8888,  identiy.ServerIP, function(){
                console.log('CONNECTED ');

            });
        });

    });
});

module.exports = app;

function ab2str(buf) {
    return String.fromCharCode.apply(null, new Uint16Array(buf));
}
