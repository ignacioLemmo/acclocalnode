(function(serverConfig){

    var jsonfile = require('jsonfile');
    var util = require('util');

    var request = require('request');
    var Q = require('q');

    var file = 'config-server.json'

    serverConfig.isConfig=function(){

        var deferred = Q.defer();

        jsonfile.readFile(file, function(err, obj) {

            if(obj){

                deferred.resolve(obj)

            }
            else{

                deferred.reject(obj)
            }
        });

        return deferred.promise;

    }

    serverConfig.CheckUserData = function (host,username,appid,homeid) {

        var deferred = Q.defer();

        var options={
            url:host+'/api/checkuser/'+username+'/'+appid+'/'+homeid+'/localserver'
        }

        request(options, function (error, response, body) {

            if (error) {
                deferred.reject(error)
            }

            if (response.statusCode !== 200) {
                deferred.reject(response)
            }

            if (!error && response.statusCode == 200) {
                deferred.resolve(body)
            }
        })

        return deferred.promise;
    }


})(module.exports);