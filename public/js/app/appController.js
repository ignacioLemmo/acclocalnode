(function(){
    'use strict';

    angular.module('lsApp').controller('localSController',['$scope','$http',function($scope, $http){

        var url="http://localhost:3000";
        var socket = io('http://localhost:3000');
        $scope.user={};
        $scope.devices = [];
        $scope.isBusy=false;
        $scope.msj="";

        $scope.isConfig=false;

        $scope.user.code="";

        $http.get(url+'/isconfig').then(
            function(res){
                $scope.isBusy=true;
                if(res.data.config == 'yes'){
                    $scope.isConfig=true;
                }
                if(res.data.config == 'no'){

                    $scope.isConfig=false;
                }
                if(res.data.config == 'error'){
                    alert("Error al configurar");
                }

            },
            function(){
                $scope.isBusy=false;
            }).
        then(function(){
            $scope.isBusy=false;
        });


        $http.get(url+'/tokenvalid').then(
            function(res){
                $scope.isBusy=true;
                if(res.data.token == 'yes'){

                    alert("Token no valido, ingrese nuevamente el codigo de seguiridad");
                    $scope.isConfig=false;

                }
                if(res.data.token == 'no'){
                    $scope.isConfig=true;

                }
                if(res.data.token == 'error'){
                    alert("Error al validar token");
                }

            },
            function(){
                $scope.isBusy=false;
                alert("Se necesita token");
            }).
            then(function(){
            $scope.isBusy=false;
        });

        $scope.SendCode=function(){
            $scope.isBusy=true;
            $http.get(url+'/config/'+$scope.user.code+'/ip/'+$scope.user.ip).
            then(
                function(resul){

                    if(resul.data.token=='ok'){

                        alert('Configuración realizada con exito reinicie el servidor para que tengan efecto los cambios')
                        $scope.isConfig=true;
                    }

                    else{

                    alert('Error al configurar el servidor local')
                    $scope.isConfig=false;

                    }

            },function(resul){

                    alert("Se produjo un error");

            }).then(function(){
                $scope.isBusy=false;
            });;

        }

        $scope.getDevices=function(){
            $scope.isBusy=true;
            $http.get(url+'/devices').
            then(
                function(resul){
                    angular.copy(resul.data, $scope.devices);
                    $scope.isBusy=false;
                },
                function(resul){

                    alert("Se produjo un error");
                }).
            then(function(){
                $scope.isBusy=false;
            });;

        }

        //SOCKET

        socket.on('connectionID',function(data){

                $scope.msj=data.msj


        });

    }]);

})();
