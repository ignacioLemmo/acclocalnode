﻿(function (webSocket) {
    
    var socketClient = require('socket.io-client');
    var _wsClient = null;
    
    

    webSocket.connect = function (url) { 
    
        _wsClient = socketClient.connect(url);

    }
    
    
    webSocket.getWsClient = function () { 
    
        return _wsClient;

    }

    webSocket.emit = function (action,message) {
        
        _wsClient.emit(action, message);
         
    };

})(module.exports);